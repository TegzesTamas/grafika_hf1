//=============================================================================================
// Szamitogepes grafika hazi feladat keret. Ervenyes 2017-tol.
// A //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// sorokon beluli reszben celszeru garazdalkodni, mert a tobbit ugyis toroljuk.
// A beadott program csak ebben a fajlban lehet, a fajl 1 byte-os ASCII karaktereket tartalmazhat.
// Tilos:
// - mast "beincludolni", illetve mas konyvtarat hasznalni
// - faljmuveleteket vegezni a printf-et kiv�ve
// - new operatort hivni a lefoglalt adat korrekt felszabaditasa nelkul
// - felesleges programsorokat a beadott programban hagyni
// - felesleges kommenteket a beadott programba irni a forrasmegjelolest kommentjeit kiveve
// ---------------------------------------------------------------------------------------------
// A feladatot ANSI C++ nyelvu forditoprogrammal ellenorizzuk, a Visual Studio-hoz kepesti elteresekrol
// es a leggyakoribb hibakrol (pl. ideiglenes objektumot nem lehet referencia tipusnak ertekul adni)
// a hazibeado portal ad egy osszefoglalot.
// ---------------------------------------------------------------------------------------------
// A feladatmegoldasokban csak olyan OpenGL/GLUT fuggvenyek hasznalhatok, amelyek az oran a feladatkiadasig elhangzottak 
//
// NYILATKOZAT
// ---------------------------------------------------------------------------------------------
// Nev    : Tegzes Tam�s
// Neptun : JA4SCB
// ---------------------------------------------------------------------------------------------
// ezennel kijelentem, hogy a feladatot magam keszitettem, es ha barmilyen segitseget igenybe vettem vagy
// mas szellemi termeket felhasznaltam, akkor a forrast es az atvett reszt kommentekben egyertelmuen jeloltem.
// A forrasmegjeloles kotelme vonatkozik az eloadas foliakat es a targy oktatoi, illetve a
// grafhazi doktor tanacsait kiveve barmilyen csatornan (szoban, irasban, Interneten, stb.) erkezo minden egyeb
// informaciora (keplet, program, algoritmus, stb.). Kijelentem, hogy a forrasmegjelolessel atvett reszeket is ertem,
// azok helyessegere matematikai bizonyitast tudok adni. Tisztaban vagyok azzal, hogy az atvett reszek nem szamitanak
// a sajat kontribucioba, igy a feladat elfogadasarol a tobbi resz mennyisege es minosege alapjan szuletik dontes.
// Tudomasul veszem, hogy a forrasmegjeloles kotelmenek megsertese eseten a hazifeladatra adhato pontokat
// negativ elojellel szamoljak el es ezzel parhuzamosan eljaras is indul velem szemben.
//=============================================================================================
#define _USE_MATH_DEFINES
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <vector>

#if defined(__APPLE__)
#include <GLUT/GLUT.h>
#include <OpenGL/gl3.h>
#include <OpenGL/glu.h>
#else
#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__)
#include <windows.h>
#endif
#include <GL/glew.h>		// must be downloaded 
#include <GL/freeglut.h>	// must be downloaded unless you have an Apple
#endif


const unsigned int windowWidth = 600, windowHeight = 600;

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// You are supposed to modify the code from here...
// OpenGL major and minor versions
int majorVersion = 3, minorVersion = 3;

const int N = 3;
//float heights[N][N] = {0};

const int M = 100;

//float heights[N][N] = {
//	1.0f, 0.1f, 0.5f, 0.2f,
//	0.1f, 0.5f, 0.2f, 0.1f,
//	1.0f, 0.8f, 1.0f, 0.5f,
//	0.8f, 0.8f, 0.5f, 0.0f
//};

float heights[N][N] = {
	0.0f, 0.0f, 0.0f,
	0.0f, 10.0f, 0.0f,
	0.0f, 0.0f, 0.0f
};


float binomial(int n, int k){
	float choose = 1;
	for(int j = 1; j <= k; j++)
		choose *= (float) (n - j + 1) / j;
	return choose;
}

float bezierHeight(float x, float y){
	float ret = 0;
	for(int i = 0; i < N; ++i){
		for(int j = 0; j < N; ++j){
			ret += heights[i][j] * binomial(N - 1, i) * binomial(N - 1, j) * pow(x, i) * pow(1 - x, N - 1 - i) * pow(y, j) * pow(1 - y, N - 1 - j);
		}
	}
	return ret;
}

float getRedForHeight(float height){
	if(height < .5)
		return 2 * (0 * (0.5f - height) + 1 * height);
	else
		return 2 * (1 * (1 - height) + (height - 0.5f) *0.54f);
}

float getGreenForHeight(float height){
	if(height < .5)
		return 2 * (.5f * (.5f - height) + 1 * height);
	else
		return 2 * (1 * (1 - height) + (height - .5f) *0.26f);
}

float getBlueForHeight(float height){
	if(height < .5)
		return 0;
	else
		return 2 * (0.1f * (1 - height) + (height - .5f) *0.07f);
}

void getErrorInfo(unsigned int handle){
	int logLen;
	glGetShaderiv(handle, GL_INFO_LOG_LENGTH, &logLen);
	if(logLen > 0){
		char * log = new char[logLen];
		int written;
		glGetShaderInfoLog(handle, logLen, &written, log);
		printf("Shader log:\n%s", log);
		delete[] log;
	}
}

// check if shader could be compiled
void checkShader(unsigned int shader, char * message){
	int OK;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &OK);
	if(!OK){
		printf("%s!\n", message);
		getErrorInfo(shader);
	}
}

// check if shader could be linked
void checkLinking(unsigned int program){
	int OK;
	glGetProgramiv(program, GL_LINK_STATUS, &OK);
	if(!OK){
		printf("Failed to link shader program!\n");
		getErrorInfo(program);
	}
}

// vertex shader in GLSL
const char * vertexSource = R"(
	#version 330
    precision highp float;

	uniform mat4 MVP;			// Model-View-Projection matrix in row-major format

	layout(location = 0) in vec2 vertexPosition;	// Attrib Array 0
	layout(location = 1) in vec3 vertexColor;	    // Attrib Array 1
	out vec3 color;									// output attribute

	void main() {
		color = vertexColor;														// copy color from input to output
		gl_Position = vec4(vertexPosition.x, vertexPosition.y, 0, 1) * MVP; 		// transform to clipping space
	}
)";

// fragment shader in GLSL
const char * fragmentSource = R"(
	#version 330
    precision highp float;

	in vec3 color;				// variable input: interpolated color of vertex shader
	out vec4 fragmentColor;		// output that goes to the raster memory as told by glBindFragDataLocation

	void main() {
		fragmentColor = vec4(color, 1); // extend RGB to RGBA
	}
)";

// row-major matrix 4x4
struct mat4{
	float m[4][4];
public:
	mat4(){}
	mat4(float m00, float m01, float m02, float m03,
		float m10, float m11, float m12, float m13,
		float m20, float m21, float m22, float m23,
		float m30, float m31, float m32, float m33){
		m[0][0] = m00; m[0][1] = m01; m[0][2] = m02; m[0][3] = m03;
		m[1][0] = m10; m[1][1] = m11; m[1][2] = m12; m[1][3] = m13;
		m[2][0] = m20; m[2][1] = m21; m[2][2] = m22; m[2][3] = m23;
		m[3][0] = m30; m[3][1] = m31; m[3][2] = m32; m[3][3] = m33;
	}

	mat4 operator*(const mat4& right) const{
		mat4 result;
		for(int i = 0; i < 4; i++){
			for(int j = 0; j < 4; j++){
				result.m[i][j] = 0;
				for(int k = 0; k < 4; k++) result.m[i][j] += m[i][k] * right.m[k][j];
			}
		}
		return result;
	}
	operator float*(){ return &m[0][0]; }
};


// 3D point in homogeneous coordinates
class vec4{
public:
	float v[4];

	vec4(float x = 0, float y = 0, float z = 0, float w = 1){
		v[0] = x; v[1] = y; v[2] = z; v[3] = w;
	}

	float operator[] (const size_t i) const{
		return v[i];
	}

	float& operator[] (const size_t i){
		return v[i];
	}

	vec4 operator*(const mat4& mat) const{
		vec4 result;
		for(int j = 0; j < 4; j++){
			result.v[j] = 0;
			for(int i = 0; i < 4; i++) result.v[j] += v[i] * mat.m[i][j];
		}
		return result;
	}

	void normalize(){
		for(int i = 0; i < 4; ++i){
			v[i] /= v[3];
		}
	}
};

class vec3{
public:
	float v[3];
	vec3(float x = 0, float y = 0, float z = 0){
		v[0] = x; v[1] = y; v[2] = z;
	}
	vec3(const vec4& a){
		for(int i = 0; i < 3; ++i){
			v[i] = a[i] / a[3];
		}
	}

	float operator[] (const size_t i) const{
		return v[i];
	}

	float& operator[] (const size_t i){
		return v[i];
	}

	vec3 operator*(const mat4& rhs) const{
		vec4 asdf = vec4(v[0], v[1], v[2], 1) * rhs;
		return vec3(asdf);
	}

	vec3 operator+(const vec3& rhs) const{
		vec3 res = vec3();
		for(int i = 0; i < 3; ++i){
			res[i] = rhs.v[i] + v[i];
		}
		return res;
	}

	vec3 operator*(float lambda) const{
		vec3 res = vec3();
		for(int i = 0; i < 3; ++i){
			res.v[i] = v[i] * lambda;
		}
		return res;
	}

	vec3& operator+=(const vec3& rhs){
		for(int i = 0; i < 3; ++i){
			v[i] += rhs.v[i];
		}
		return *this;
	}

	vec3& operator*=(float lambda){
		for(int i = 0; i < 3; ++i){
			v[i] *= lambda;
		}
		return *this;
	}

	vec3 operator-(const vec3& rhs) const{
		vec3 res = vec3();
		for(int i = 0; i < 3; ++i){
			res[i] = v[i] - rhs[i];
		}
		return res;
	}

	float abs() const{
		float sqrSum = 0;
		for(int i = 0; i < 3; ++i){
			sqrSum += v[i] * v[i];
		}
		return sqrt(sqrSum);
	}

	float horizontalAbs() const{
		return sqrt(v[0] * v[0] + v[1] * v[1]);
	}
};


// 2D camera
struct Camera{
	float wCx, wCy;	// center in world coordinates
	float wWx, wWy;	// width and height in world coordinates
public:
	Camera(){
		Animate(0);
	}

	mat4 V(){ // view matrix: translates the center to the origin
		return mat4(1, 0, 0, 0,
			0, 1, 0, 0,
			0, 0, 1, 0,
			-wCx, -wCy, 0, 1);
	}

	mat4 P(){ // projection matrix: scales it to be a square of edge length 2
		return mat4(2 / wWx, 0, 0, 0,
			0, 2 / wWy, 0, 0,
			0, 0, 1, 0,
			0, 0, 0, 1);
	}

	mat4 Vinv(){ // inverse view matrix
		return mat4(1, 0, 0, 0,
			0, 1, 0, 0,
			0, 0, 1, 0,
			wCx, wCy, 0, 1);
	}

	mat4 Pinv(){ // inverse projection matrix
		return mat4(wWx / 2, 0, 0, 0,
			0, wWy / 2, 0, 0,
			0, 0, 1, 0,
			0, 0, 0, 1);
	}

	void Animate(float t){
		wWx = 1;
		wWy = 1;
		wCx = 0.5f * wWx;
		wCy = 0.5f * wWy;
	}
};

// 2D camera
Camera camera;

// handle of the shader program
unsigned int shaderProgram;

class LagrangeCurve{
	static const int divisions = 100;
	GLuint vao = 0, vbo = 0;        // vertex array object, vertex buffer object
	std::vector<vec3>  cps;	// control points 
	std::vector<float> ts; 	// parameter (knot) values
	float minT = 0, maxT = 0;
	float length = 0;

	float L(size_t i, float t) const{
		float Li = 1.0f;
		for(size_t j = 0; j < ts.size(); ++j)
			if(j != i)
				Li *= (t - ts[j]) / (ts[i] - ts[j]);
		return Li;
	}

	float LDerivative(size_t i, float t) const{
		float ret = 0;
		for(size_t k = 0; k < ts.size(); ++k){
			if(k != i){
				float prod = 1;
				for(size_t j = 0; j < ts.size(); ++j){
					if(j != i){
						if(j == k){
							prod /= (ts[i] - ts[j]);
						}
						else{
							prod *= (t - ts[j]) / (ts[i] - ts[j]);
						}
					}
				}
				ret += prod;
			}
		}
		return ret;
	}
public:

	void clear(){
		cps.clear();
		ts.clear();
	}

	vec3 r(float t) const{
		t = t + minT;
		if(t < 0) t = 0;
		if(t > maxT) t = maxT;
		if(cps.size() == 0)
			return vec3();
		if(cps.size() == 1)
			return cps[0];
		vec3 rr = vec3();
		for(size_t i = 0; i < cps.size(); ++i){
			rr += (cps[i] * L(i, t));
		}
		return rr;
	}

	vec3 rDerivative(float t) const{
		t = t + minT;
		if(t < minT) t = 0;
		if(t > maxT) t = maxT;
		if(cps.size() == 0)
			return vec3();
		if(cps.size() == 1)
			return vec3();
		vec3 rr = vec3();
		for(size_t i = 0; i < cps.size(); ++i){
			rr += (cps[i] * LDerivative(i, t));
		}
		return rr;
	}

	void AddPoint(float x, float y, float t){
		if(cps.size() >= 20){
			printf("tul sok pont");
			return;
		}
		if(cps.size() == 0)
			minT = t;
		vec4 horizontalCP = vec4(x, y, 0,1) * (camera.Pinv() * camera.Vinv());
		vec3 cp = vec3(horizontalCP);
		cp += vec3(0, 0, bezierHeight(cp[0], cp[1]));
		printf("added %f, %f, %f\n", cp[0], cp[1], cp[2]);
		if(t > maxT) maxT = t;
		cps.push_back(cp);
		ts.push_back(t);
		createPath();
		glutPostRedisplay();
	}

	void Create(){
		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);

		glGenBuffers(1, &vbo); // Generate 1 vertex buffer object
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		// Enable the vertex attribute arrays
		glEnableVertexAttribArray(0);  // attribute array 0
		glEnableVertexAttribArray(1);  // attribute array 1
									   // Map attribute array 0 to the vertex data of the interleaved vbo
		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), reinterpret_cast<void*>(0)); // attribute array, components/attribute, component type, normalize?, stride, offset
																										// Map attribute array 1 to the color data of the interleaved vbo
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), reinterpret_cast<void*>(2 * sizeof(float)));
	}

	void createPath(){
		glBindVertexArray(vao);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		//float* vertexData = new float[divisions * cps.size()];
		static float vertexData[20 * divisions * 5];
		float t = 0;
		length = 0;
		float step = (maxT - minT) / divisions / (cps.size() - 1);
		int index = 0;
		vec3 prevR = this->r(t);
		vertexData[index++] = prevR[0];
		vertexData[index++] = prevR[1];
		//Colors
		for(int i = 0; i < 3; ++i){
			vertexData[index++] = 1;
		}
		t += step;
		for(size_t i = 1; i < divisions*(cps.size() - 1); ++i){
			vec3 r = this->r(t);
			//length += (r - prevR).abs();
			vertexData[index++] = r[0];
			vertexData[index++] = r[1];
			//Colors
			for(size_t i = 0; i < 3; ++i){
				vertexData[index++] = 1;
			}
			t += step;
			prevR = r;
		}
		vertexData[index++] = cps[cps.size() - 1][0];
		vertexData[index++] = cps[cps.size() - 1][1];
		for(int i = 0; i < 3; ++i){
			vertexData[index++] = 1;
		}
		printf("index: %d\n", index);
		glBufferData(GL_ARRAY_BUFFER, index * sizeof(float), vertexData, GL_DYNAMIC_DRAW);
		//delete[] vertexData;
		printf("%f\n", length);
	}

	void Draw() const{
		if(cps.size() > 0){
			mat4 VPTransform = camera.V() * camera.P();

			int location = glGetUniformLocation(shaderProgram, "MVP");
			if(location >= 0) glUniformMatrix4fv(location, 1, GL_TRUE, VPTransform);
			else printf("uniform MVP cannot be set\n");

			glBindVertexArray(vao);
			glDrawArrays(GL_LINE_STRIP, 0, (cps.size() - 1)*divisions + 1);
		}
	}
};


class TriangleWeb{
private:
	GLuint vao;
	float startX, startY;
	float sizeX, sizeY;
public:
	TriangleWeb(float startX, float startY, float sizeX, float sizeY) :startX(startX), startY(startY), sizeX(sizeX), sizeY(sizeY), vao(0){}
	void Create(){
		glGenVertexArrays(1, &vao);	// create 1 vertex array object
		glBindVertexArray(vao);		// make it active

		unsigned int vbo[2];		// vertex buffer objects
		glGenBuffers(2, &vbo[0]);	// Generate 2 vertex buffer objects

									// vertex coordinates: vbo[0] -> Attrib Array 0 -> vertexPosition of the vertex shader
		glBindBuffer(GL_ARRAY_BUFFER, vbo[0]); // make it active, it is an array

		static float vertexCoords[M*(M + 2) * 4];
		int index = 0;
		float x = startX, y = startY;
		for(int i = 0; i < M - 1; ++i){
			//Send first coordinates one more time, so that the first triangle drawn is based on the points given in this column
			vertexCoords[index++] = x;
			vertexCoords[index++] = y;
			for(int j = 0; j < M; ++j){
				//Left point in the grid
				vertexCoords[index++] = x;
				vertexCoords[index++] = y;
				//Right point in the grid
				x += sizeX;
				vertexCoords[index++] = x;
				vertexCoords[index++] = y;
				//Move up, and left
				x -= sizeX;
				y += sizeY;
			}
			//Send last coordinates one more time, so that the first triangle drawn is based on the points given in the next column
			y -= sizeY;
			x += sizeX;
			vertexCoords[index++] = x;
			vertexCoords[index++] = y;
			//Go back to the bottom of the column
			y = startY;
		}

		glBufferData(GL_ARRAY_BUFFER,      // copy to the GPU
			sizeof(vertexCoords), // number of the vbo in bytes
			vertexCoords,		   // address of the data array on the CPU
			GL_STATIC_DRAW);	   // copy to that part of the memory which is not modified 
								   // Map Attribute Array 0 to the current bound vertex buffer (vbo[0])
		glEnableVertexAttribArray(0);
		// Data organization of Attribute Array 0 
		glVertexAttribPointer(0,			// Attribute Array 0
			2, GL_FLOAT,  // components/attribute, component type
			GL_FALSE,		// not in fixed point format, do not normalized
			0, NULL);     // stride and offset: it is tightly packed

						  // vertex colors: vbo[1] -> Attrib Array 1 -> vertexColor of the vertex shader
		glBindBuffer(GL_ARRAY_BUFFER, vbo[1]); // make it active, it is an array

		printf("vertices done\n");



		static float vertexColors[M*(M + 2) * 6];
		index = 0;
		x = startX;
		y = startY;
		float height;
		for(int i = 0; i < M - 1; ++i){
			height = bezierHeight(x, y);
			vertexColors[index++] = getRedForHeight(height);
			vertexColors[index++] = getGreenForHeight(height);
			vertexColors[index++] = getBlueForHeight(height);
			for(int j = 0; j < M; ++j){
				height = bezierHeight(x, y);
				vertexColors[index++] = getRedForHeight(height);
				vertexColors[index++] = getGreenForHeight(height);
				vertexColors[index++] = getBlueForHeight(height);
				x += sizeX;
				height = bezierHeight(x, y);
				vertexColors[index++] = getRedForHeight(height);
				vertexColors[index++] = getGreenForHeight(height);
				vertexColors[index++] = getBlueForHeight(height);
				x -= sizeX;
				y += sizeY;
			}
			x += sizeX;
			height = bezierHeight(x, y);
			vertexColors[index++] = getRedForHeight(height);
			vertexColors[index++] = getGreenForHeight(height);
			vertexColors[index++] = getBlueForHeight(height);
			y = startY;
		}
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertexColors), vertexColors, GL_STATIC_DRAW);	// copy to the GPU

																							// Map Attribute Array 1 to the current bound vertex buffer (vbo[1])
		glEnableVertexAttribArray(1);  // Vertex position
									   // Data organization of Attribute Array 1
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, NULL); // Attribute Array 1, components/attribute, component type, normalize?, tightly packed


		printf("colors done\n");
	}

	void Draw(){
		mat4 Mscale(1, 0, 0, 0,
			0, 1, 0, 0,
			0, 0, 0, 0,
			0, 0, 0, 1); // model matrix

		mat4 Mtranslate(1, 0, 0, 0,
			0, 1, 0, 0,
			0, 0, 0, 0,
			0, 0, 0, 1); // model matrix

		mat4 MVPTransform = Mscale * Mtranslate * camera.V() * camera.P();

		// set GPU uniform matrix variable MVP with the content of CPU variable MVPTransform
		int location = glGetUniformLocation(shaderProgram, "MVP");
		if(location >= 0) glUniformMatrix4fv(location, 1, GL_TRUE, MVPTransform); // set uniform variable MVP to the MVPTransform
		else printf("uniform MVP cannot be set\n");

		glBindVertexArray(vao);	// make the vao and its vbos active playing the role of the data source
		glDrawArrays(GL_TRIANGLE_STRIP, 0, M*(2 * M + 4));
	}
};

class Cyclist{
	unsigned int cyclistVao;	// vertex array object id
	unsigned int triangleVao;
	float sx, sy;		// scaling
	float wTx, wTy;		// translation
	float steepness;
	float posx = .9f, posy = .5f;
	const LagrangeCurve& lagrange;
	float minT = 0;
	boolean moving = false;
	float cosinus = 1;
	float sinus = 0;

public:
	Cyclist(const LagrangeCurve& lagrange) :lagrange(lagrange){}

	void startMoving(){
		minT = glutGet(GLUT_ELAPSED_TIME);
		moving = true;
	}

	void Create(){
		//glGenVertexArrays(1, &vao);
		//glBindVertexArray(vao);

		//glGenBuffers(1, &vbo); // Generate 1 vertex buffer object
		//glBindBuffer(GL_ARRAY_BUFFER, vbo);
		//// Enable the vertex attribute arrays
		//glEnableVertexAttribArray(0);  // attribute array 0
		//glEnableVertexAttribArray(1);  // attribute array 1
		//							   // Map attribute array 0 to the vertex data of the interleaved vbo
		//glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), reinterpret_cast<void*>(0)); // attribute array, components/attribute, component type, normalize?, stride, offset
		//																								// Map attribute array 1 to the color data of the interleaved vbo
		//glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), reinterpret_cast<void*>(2 * sizeof(float)));

		glGenVertexArrays(1, &triangleVao);
		glBindVertexArray(triangleVao);
		unsigned int triangleVbo;
		glGenBuffers(1, &triangleVbo);
		glBindBuffer(GL_ARRAY_BUFFER, triangleVbo);

		glEnableVertexAttribArray(0);  // attribute array 0
		glEnableVertexAttribArray(1);  // attribute array 1

		static float triangleData[] =
		{0,0, 1,1,1,
		 1,0, 1,1,1,
		 1,1, 1,1,1};

		glBufferData(GL_ARRAY_BUFFER, sizeof(triangleData), triangleData, GL_STATIC_DRAW);
		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), reinterpret_cast<void*>(0));
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), reinterpret_cast<void*>(2 * sizeof(float)));


		glGenVertexArrays(1, &cyclistVao);	// create 1 vertex array object
		glBindVertexArray(cyclistVao);		// make it active

		unsigned int vbo[2];		// vertex buffer objects
		glGenBuffers(2, &vbo[0]);	// Generate 2 vertex buffer objects

									// vertex coordinates: vbo[0] -> Attrib Array 0 -> vertexPosition of the vertex shader
		glBindBuffer(GL_ARRAY_BUFFER, vbo[0]); // make it active, it is an array
		static float vertexCoords[] = {-1,-1,  0,0,  1,0,  -1,1};	// vertex data on the CPU
		glBufferData(GL_ARRAY_BUFFER,      // copy to the GPU
			sizeof(vertexCoords), // number of the vbo in bytes
			vertexCoords,		   // address of the data array on the CPU
			GL_STATIC_DRAW);	   // copy to that part of the memory which is not modified 
								   // Map Attribute Array 0 to the current bound vertex buffer (vbo[0])
		glEnableVertexAttribArray(0);
		// Data organization of Attribute Array 0 
		glVertexAttribPointer(0,			// Attribute Array 0
			2, GL_FLOAT,  // components/attribute, component type
			GL_FALSE,		// not in fixed point format, do not normalized
			0, NULL);     // stride and offset: it is tightly packed

						  // vertex colors: vbo[1] -> Attrib Array 1 -> vertexColor of the vertex shader
		glBindBuffer(GL_ARRAY_BUFFER, vbo[1]); // make it active, it is an array
		static float vertexColors[] = {1, 1, 0,  1, 0, 0,  0, 1, 0,  1, 1, 0};	// vertex data on the CPU
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertexColors), vertexColors, GL_STATIC_DRAW);	// copy to the GPU

																							// Map Attribute Array 1 to the current bound vertex buffer (vbo[1])
		glEnableVertexAttribArray(1);  // Vertex position
									   // Data organization of Attribute Array 1
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, NULL); // Attribute Array 1, components/attribute, component type, normalize?, tightly packed


	}

	void drawTriangle() const{
		mat4 MScale(
			0.05f, 0, 0, 0,
			0, 0.05f*steepness, 0, 0,
			0, 0, 0, 0,
			0, 0, 0, 1
		);

		mat4 MTranslate(
			1, 0, 0, 0,
			0, 1, 0, 0,
			0, 0, 0, 0,
			posx, posy, 0, 1
		);

		mat4 MVPTransform = MScale * MTranslate * camera.V() * camera.P();

		// set GPU uniform matrix variable MVP with the content of CPU variable MVPTransform
		int location = glGetUniformLocation(shaderProgram, "MVP");
		if(location >= 0) glUniformMatrix4fv(location, 1, GL_TRUE, MVPTransform); // set uniform variable MVP to the MVPTransform
		else printf("uniform MVP cannot be set\n");

		glBindVertexArray(triangleVao);	// make the vao and its vbos active playing the role of the data source
		glDrawArrays(GL_TRIANGLES, 0, 3);	// draw a single triangle with vertices defined in vao
	}

	void Draw(){
		mat4 Mscale(
			sx, 0, 0, 0,
			0, sy, 0, 0,
			0, 0, 0, 0,
			0, 0, 0, 1); // model matrix

		mat4 Mtranslate(
			1, 0, 0, 0,
			0, 1, 0, 0,
			0, 0, 0, 0,
			wTx, wTy, 0, 1); // model matrix

		mat4 MRotate(
			cosinus, sinus, 0, 0,
			-sinus, cosinus, 0, 0,
			0, 0, 0, 0,
			0, 0, 0, 1);



		mat4 MVPTransform = Mscale * MRotate * Mtranslate * camera.V() * camera.P();

		// set GPU uniform matrix variable MVP with the content of CPU variable MVPTransform
		int location = glGetUniformLocation(shaderProgram, "MVP");
		if(location >= 0) glUniformMatrix4fv(location, 1, GL_TRUE, MVPTransform); // set uniform variable MVP to the MVPTransform
		else printf("uniform MVP cannot be set\n");

		glBindVertexArray(cyclistVao);	// make the vao and its vbos active playing the role of the data source
		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);	// draw a single triangle with vertices defined in vao
		drawTriangle();

	}
	void Animate(float t){
		t = t - minT;
		if(moving){
			vec3 r = lagrange.r(t);
			vec3 rd = lagrange.rDerivative(t);
			sx = .05f *rd.horizontalAbs() / rd.abs();
			sy = .05f;
			//wTx = .5f;
			//wTy = .5f;
			wTx = r[0];
			wTy = r[1];
			cosinus = rd[0] / rd.horizontalAbs();
			sinus = rd[1] / rd.horizontalAbs();
			//printf("%f %f sinus: %f cosinus: %f\n", rd[0], rd[1], sinus, cosinus);

			steepness = rd[2] / rd.horizontalAbs();

			glutPostRedisplay();
		}
	}

	void stopMoving(){
		moving = false;
	}
};

// The virtual world: collection of two objects
TriangleWeb triangleWeb(0, 0, 1.0f / (M - 1.0f), 1.0f / (M - 1.0f));
LagrangeCurve lagrange;
Cyclist cyclist(lagrange);

// Initialization, create an OpenGL context
void onInitialization(){
	glViewport(0, 0, windowWidth, windowHeight);

	// Create objects by setting up their vertex data on the GPU
	triangleWeb.Create();
	lagrange.Create();
	cyclist.Create();

	// Create vertex shader from string
	unsigned int vertexShader = glCreateShader(GL_VERTEX_SHADER);
	if(!vertexShader){
		printf("Error in vertex shader creation\n");
		exit(1);
	}
	glShaderSource(vertexShader, 1, &vertexSource, NULL);
	glCompileShader(vertexShader);
	checkShader(vertexShader, "Vertex shader error");

	// Create fragment shader from string
	unsigned int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	if(!fragmentShader){
		printf("Error in fragment shader creation\n");
		exit(1);
	}
	glShaderSource(fragmentShader, 1, &fragmentSource, NULL);
	glCompileShader(fragmentShader);
	checkShader(fragmentShader, "Fragment shader error");

	// Attach shaders to a single program
	shaderProgram = glCreateProgram();
	if(!shaderProgram){
		printf("Error in shader program creation\n");
		exit(1);
	}
	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, fragmentShader);

	// Connect the fragmentColor to the frame buffer memory
	glBindFragDataLocation(shaderProgram, 0, "fragmentColor");	// fragmentColor goes to the frame buffer memory

	// program packaging
	glLinkProgram(shaderProgram);
	checkLinking(shaderProgram);
	// make this program run
	glUseProgram(shaderProgram);
}

void onExit(){
	glDeleteProgram(shaderProgram);
	printf("exit");
}

// Window has become invalid: Redraw
void onDisplay(){
	glClearColor(0, 0, 0, 0);							// background color 
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear the screen

	triangleWeb.Draw();
	lagrange.Draw();
	cyclist.Draw();
	glutSwapBuffers();									// exchange the two buffers
}

// Key of ASCII code pressed
void onKeyboard(unsigned char key, int pX, int pY){
	if(key == 'd') glutPostRedisplay();         // if d, invalidate display, i.e. redraw
	if(key == ' ') cyclist.startMoving();
	if(key == 's') cyclist.stopMoving();
	if(key == 'c') lagrange.clear();
}

// Key of ASCII code released
void onKeyboardUp(unsigned char key, int pX, int pY){

}

// Mouse click event
void onMouse(int button, int state, int pX, int pY){
	if(button == GLUT_LEFT_BUTTON && state == GLUT_DOWN){  // GLUT_LEFT_BUTTON / GLUT_RIGHT_BUTTON and GLUT_DOWN / GLUT_UP
		float cX = 2.0f * pX / windowWidth - 1;	// flip y axis
		float cY = 1.0f - 2.0f * pY / windowHeight;
		lagrange.AddPoint(cX, cY, glutGet(GLUT_ELAPSED_TIME));
		glutPostRedisplay();     // redraw
	}
}

// Move mouse with key pressed
void onMouseMotion(int pX, int pY){}

// Idle event indicating that some time elapsed: do animation here
void onIdle(){
	float time = glutGet(GLUT_ELAPSED_TIME); // elapsed time since the start of the program
	camera.Animate(time);					// animate the camera
	cyclist.Animate(time);					// animate the triangle object
	glutPostRedisplay();					// redraw the scene
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Do not touch the code below this line

int main(int argc, char * argv[]){
	glutInit(&argc, argv);
#if !defined(__APPLE__)
	glutInitContextVersion(majorVersion, minorVersion);
#endif
	glutInitWindowSize(windowWidth, windowHeight);				// Application window is initially of resolution 600x600
	glutInitWindowPosition(100, 100);							// Relative location of the application window
#if defined(__APPLE__)
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH | GLUT_3_3_CORE_PROFILE);  // 8 bit R,G,B,A + double buffer + depth buffer
#else
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
#endif
	glutCreateWindow(argv[0]);

#if !defined(__APPLE__)
	glewExperimental = true;	// magic
	glewInit();
#endif

	printf("GL Vendor    : %s\n", glGetString(GL_VENDOR));
	printf("GL Renderer  : %s\n", glGetString(GL_RENDERER));
	printf("GL Version (string)  : %s\n", glGetString(GL_VERSION));
	glGetIntegerv(GL_MAJOR_VERSION, &majorVersion);
	glGetIntegerv(GL_MINOR_VERSION, &minorVersion);
	printf("GL Version (integer) : %d.%d\n", majorVersion, minorVersion);
	printf("GLSL Version : %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	onInitialization();

	glutDisplayFunc(onDisplay);                // Register event handlers
	glutMouseFunc(onMouse);
	glutIdleFunc(onIdle);
	glutKeyboardFunc(onKeyboard);
	glutKeyboardUpFunc(onKeyboardUp);
	glutMotionFunc(onMouseMotion);

	glutMainLoop();
	onExit();
	return 1;
}

